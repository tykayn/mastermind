import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Mastermind TK';
  config = {
    player: 1,
    autoRun: 0,
    randomGoal: 1,
    debug: 0,
    turns: 12,
    sequenceLength: 4,
    doubleColors: 1,
    couleurs: ['yellow', 'violet', 'green', 'blue', 'red', 'orange', 'white', 'fuschia'],
    result : [],
    sequence : [],
    altColors: false,
    lines : [],
    won : false,
    loose : false,
    sequenceAdverse : ["blue", "yellow", "red", "green"],
  }
}
