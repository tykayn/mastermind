import { TestBed } from '@angular/core/testing';

import { IaServiceService } from './ia-service.service';

describe('IaServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IaServiceService = TestBed.get(IaServiceService);
    expect(service).toBeTruthy();
  });
});
