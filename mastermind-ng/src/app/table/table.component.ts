import {Component, Input, OnInit} from '@angular/core';
import {IaServiceService} from "../ia-service.service";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  lines: any;

  @Input() config: any;

  constructor(private IA: IaServiceService) {
  }

  gagner() {
    var c, i, j, len, obj, ref;
    i = 0;
    ref = this.config.sequenceAdverse;
    for (j = 0, len = ref.length; j < len; j++) {
      c = ref[j];
      obj = {
        id: i++,
        color: c
      };
      this.config.sequence.push(obj);
    }
    return this.config.sequence;
  }

  emptyTable() {
    console.log('emptyTable');
    this.config.won = 0;
    return this.config.lines = [];
  }

  addSequence(sequence) {
    var evaluation, newSeq, obj;
    newSeq = Object.create(sequence);
    if (this.config.lengthLines === this.config.turns - 1) {
      console.log('tour max atteint');
      return false;
    }
    if (this.config.won) {
      console.log('vous avez déjà gagné');
      return false;
    }
    console.log('addSequence', newSeq);
    this.config.lengthLines = this.config.lines.length;
    evaluation = this.config.evaluate(newSeq);
    this.config.result[this.config.lengthLines] = evaluation;
    obj = {
      id: this.config.lengthLines,
      pions: newSeq
    };
    this.config.lines.push(obj);
    return evaluation;
  }

  /*
  évaluer la séquence
  et donner ses stats de réponse
   */
  evaluate(sequence) {
    var couleursAdverses, elem, evaluation, goods, i, j, k, len, len1, nearly, ref;
    goods = 0;
    nearly = 0;
    i = 0;
    couleursAdverses = [];
    ref = this.config.sequenceAdverse;
    for (j = 0, len = ref.length; j < len; j++) {
      elem = ref[j];
      couleursAdverses.push(elem.color);
    }
    console.log('___________ couleursAdverses', couleursAdverses);
    for (k = 0, len1 = sequence.length; k < len1; k++) {
      elem = sequence[k];
      console.log('___________ elem', elem.color, couleursAdverses.indexOf(elem.color));
      if (couleursAdverses[i] === elem.color) {
        goods++;
      } else if (couleursAdverses.indexOf(elem.color) !== -1) {
        nearly++;
      }
      i++;
    }
    evaluation = {
      goods: goods,
      nearly: nearly
    };
    if (evaluation.goods === this.config.sequenceLength) {
      this.config.won = 1;
      console.log('gagné');
      return evaluation;
    } else if (this.config.lines.length === this.config.turns - 1) {
      this.config.loose = 1;
      return evaluation;
    }
    if (this.config.autoRun) {
      this.config.sequence = Object.create(this.IA.wonder(evaluation, sequence));
      this.IA.dumpTree();
    }
    return evaluation;
  }

  addRandomSequence() {
    var seq;
    seq = this.randomSequence();
    this.addSequence(seq);
    return seq;
  }

  randomSequence() {
    var colorList, i, j, k, obj, randomColor, randomNb, tab;
    tab = [];
    if (this.config.doubleColors) {
      colorList = Object.create(this.config.couleurs);
      for (i = j = 1; j <= 4; i = ++j) {
        randomNb = Math.floor(Math.random() * colorList.length);
        randomColor = colorList[randomNb];
        colorList.splice(randomNb, 1);
        obj = {
          id: i,
          color: randomColor
        };
        tab.push(obj);
      }
    } else {
      for (i = k = 1; k <= 4; i = ++k) {
        randomNb = Math.floor(Math.random() * this.config.couleurs.length);
        randomColor = Object.create(this.config.couleurs[randomNb]);
        obj = {
          id: i,
          color: randomColor
        };
        tab.push(obj);
      }
    }
    return tab;
  }

  populateSequence() {
    return this.config.sequence = [
      {
        id: 0,
        color: 'blue'
      }, {
        id: 1,
        color: 'yellow'
      }, {
        id: 2,
        color: 'red'
      }, {
        id: 3,
        color: 'green'
      }
    ];
  }

  emptySequence() {
    console.info('vider la séquence');
    this.config.lengthLines = 0;
    this.config.sequence = [];
    this.config.lengthLines = 0;
    return this.config.sequence = [];
  }

  setSequence(id) {
    var seq;
    seq = Object.create(this.config.lines[id].pions);
    console.log('setSequence', seq);
    return this.config.sequence = seq;
  }

  colorUnique(color) {
    var j, len, pion, ref;
    ref = this.config.sequence;
    for (j = 0, len = ref.length; j < len; j++) {
      pion = ref[j];
      if (pion.color === color) {
        console.log('couleur non unique', color);
        return false;
      }
    }
    return true;
  }

  addColor(color) {
    var j, len, newId, pion, ref;
    if (!this.config.colorUnique(color)) {
      return false;
    }
    if (this.config.sequence.length === this.config.sequenceLength) {
      this.config.sequence.splice(0, 1);
    }
    newId = 0;
    ref = this.config.sequence;
    for (j = 0, len = ref.length; j < len; j++) {
      pion = ref[j];
      pion.id = newId;
      newId++;
    }
    return this.config.sequence.push({
      id: newId,
      color: color
    });
  }

  deleteColor(color) {
    var counter, index, j, len, lepion, pion, ref;
    counter = 0;
    ref = this.config.sequence;
    for (j = 0, len = ref.length; j < len; j++) {
      pion = ref[j];
      if (pion.color === color) {
        index = counter;
        lepion = pion;
      }
      counter++;
    }
    console.log('enlever', color, index, this.config.sequence[index]);
    return this.config.sequence.splice(index, 1);
  }

  goPlayer() {
    this.config.player = 1;
    return this.config.emptySequence();
  }

  autoRun() {
    var i, j, ref, results, suggestion;
    if (!this.config.autoRun) {
      console.log('autoRun désactivé');
      return;
    }
    console.log('autoRun pour ' + this.config.turns + ' tours');
    results = [];
    for (i = j = 1, ref = this.config.turns; 1 <= ref ? j <= ref : j >= ref; i = 1 <= ref ? ++j : --j) {
      if (!this.config.won) {
        suggestion = this.IA.suggestSequence();
        console.log('suggestion', suggestion);
        results.push(this.config.addSequence(suggestion));
      } else {
        results.push(void 0);
      }
    }
    return results;
  }

  ngOnInit() {


    this.IA.makeTree(this.config.couleurs);
    if (this.config.randomGoal) {
      this.config.sequenceAdverse = this.randomSequence();
      console.log('but aléatoire', this.config.sequenceAdverse);
    }
    if (this.config.autoRun) {
      return this.autoRun();
    }

  }

  restart() {
    this.emptyTable();
    this.autoRun();

  }

}
